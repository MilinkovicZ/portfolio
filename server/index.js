const path = require("path");
const express = require("express");
const cors = require("cors");
const nodemailer = require("nodemailer");
const bodyparser = require("body-parser");
require("dotenv").config();

const PORT = process.env.PORT;

const app = express();
app.use(express.static(path.resolve(__dirname, '../build')))
app.use(cors());
app.use(bodyparser.json())

const contactEmail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL_ADDRESS,
        pass: process.env.EMAIL_PASS
    }
})

contactEmail.verify((error) => {
    if (error)
        console.log(error)
})

app.post("/api/contact", bodyparser.urlencoded({ extended: false }), (req, res) => {
    const name = req.body.firstName + req.body.lastName;
    const email = req.body.email;
    const phone = req.body.phone;
    const message = req.body.message;

    const mail = {
        from: email,
        to: process.env.EMAIL_ADDRESS,
        subject: "Contact Form Submission - Portfolio",
        html: `<p>New message received from: ${email} !</p>
        <p>Hello Zdravko, I am: ${name}</p>
        <p></p>
        <p>${message}</p>
        <p></p>
        <p>If you have any additional questions, this is my phone number: ${phone}</p>`,
    }

    contactEmail.sendMail(mail, (error) => {
        if (error) {
            res.json(error);
        } else {
            res.json({ code: 200, status: "Message Sent" });
        }
    });
})

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../build', 'index.html'))
})

app.listen(PORT, () => {
    console.log("Listening...");
})