import NavigationBar from "./components/NavigationBar.js";
import Banner from "./components/Banner.js";
import Skills from "./components/Skills.js";
import Projects from "./components/Projects.js";
import ContactForm from "./components/ContactForm.js";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Footer from "./components/Footer.js";


function App() {
  return (
    <div className="App">
      <NavigationBar/>
      <Banner/>
      <Skills/>
      <Projects/>
      <ContactForm/>
      <Footer/>
    </div>
  );
}

export default App;