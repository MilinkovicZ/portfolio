import { Col } from "react-bootstrap";

export const Project = ({ title, description, imgUrl, GITURL}) => {
  return (
    <Col size={12} sm={6} md={3}>
      <a
        className="projectLinks"
        href={GITURL}
        target="_blank"
        rel="noreferrer">
        <div className="proj-imgbx">
          <img src={imgUrl} alt="Project" />
          <div className="proj-txtx">
            <h4>{title}</h4>
            <span>{description}</span>
          </div>
        </div>
      </a>
    </Col>
  )
}

export default Project;