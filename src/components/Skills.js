import { Col, Container, Row } from "react-bootstrap";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css"

const Skills = () => {
    const responsive = {
        superLargeDesktop: {
            breakpoint: { max: 4000, min: 3000 },
            items: 5
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1
        }
    };

    return (
        <section className="skill" id="skills">
            <Container>
                <Row>
                    <Col>
                        <div className="skill-bx">
                            <h2>Skills</h2>
                            <p>Significant skills:</p>
                            <Carousel responsive={responsive} infinite={true} className="owl-carousel owl-theme skill-slider">
                                <div className="itemSlim">
                                    <img src="./images/logos/CSharp.png" alt="C#" />
                                    <h5>C#</h5>
                                </div>
                                <div className="itemWide">
                                    <img src="./images/logos/react.png" alt="React" />
                                    <h5>ReactJS</h5>
                                </div>
                                <div className="itemDefault">
                                    <img src="./images/logos/python.png" alt="Python" />
                                    <h5>Python</h5>
                                </div>
                                <div className="itemVerySlim">
                                    <img src="./images/logos/CPlusPlus.png" alt="C++" />
                                    <h5>C++</h5>
                                </div>
                                <div className="itemVerySlim">
                                    <img src="./images/logos/CLogo.png" alt="C" />
                                    <h5>C</h5>
                                </div>                                       
                                <div className="itemVeryWide">
                                    <img src="./images/logos/javascript.png" alt="JavaScript" />
                                    <h5>JavaScript</h5>
                                </div>
                                <div className="itemDefault">
                                    <img src="./images/logos/html.png" alt="HTML" />
                                    <h5>HTML</h5>
                                </div>
                                <div className="itemVerySlim">
                                    <img src="./images/logos/css.png" alt="CSS" />
                                    <h5>CSS</h5>
                                </div>
                                <div className="itemDefault">
                                    <img src="./images/logos/azure.png" alt="Azure" />
                                    <h5>Microsoft Azure</h5>
                                </div>
                                <div className="itemMediumWide">
                                    <img src="./images/logos/docker.png" alt="Docker" />
                                    <h5>Docker</h5>
                                </div>
                            </Carousel>
                        </div>
                    </Col>
                </Row>
            </Container>
            <img className="background-image-left" src="./images/bgs/grad.png" alt="Left BG" />
        </section>
    );
}

export default Skills;