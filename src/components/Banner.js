import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { ArrowRightCircle } from "react-bootstrap-icons";
import { BrowserRouter } from "react-router-dom";
import { HashLink } from "react-router-hash-link";

const Banner = () => {
    const toRotate = ["Software Engineer", "Web Developer ", "Full Stack Developer", ".NET Developer", "Computer Engineer"];
    const [currNumber, setCurrNumber] = useState(0);
    const [isDeleting, setIsDeleting] = useState(false);
    const [text, setText] = useState('');
    const [letterTime, setLetterTime] = useState(300 - Math.random() * 100);
    const rotatePeriod = 2000;

    useEffect(() => {
        const timer = setInterval(() => {
            tick();
        }, letterTime);

        return () => { clearInterval(timer) };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [text])

    const tick = () => {
        let temp = currNumber % toRotate.length;
        let fullText = toRotate[temp];
        let updatedText = isDeleting ? fullText.substring(0, text.length - 1) : fullText.substring(0, text.length + 1);

        setText(updatedText);

        if (isDeleting)
            setLetterTime(prevTime => prevTime / 2);

        if (!isDeleting && updatedText === fullText) {
            setIsDeleting(true);
            setLetterTime(rotatePeriod);
        } else if (isDeleting && updatedText === '') {
            setIsDeleting(false);
            setCurrNumber(currNumber + 1);
            setLetterTime(500);
        }
    }

    return (
        <BrowserRouter>
            <section className="banner" id="home">
                <Container>
                    <Row className="align-items-center">
                        <Col xs={12} md={6} xl={7}>
                            <span className="tagline">
                                Welcome to my Portfolio
                            </span>
                            <h1>
                                {"Hi, I'm Zdravko"}<span className="wrap"> {text}</span>
                            </h1>
                            <p>Playing video games and messing
                                around on PC made me realize I am
                                interested in the computer science
                                field. Naturally, my goal is to start a
                                career in the IT industry and to get Master's degree in E-Business.</p>
                            <HashLink to='#connect' className="bannerHashLink">
                                <button>Let’s Connect <ArrowRightCircle size={25} /></button>
                            </HashLink>
                        </Col>
                        <Col xs={12} md={6} xl={5}>
                            <img src="./images/bgs/octopus.png" alt="Header" />
                        </Col>
                    </Row>
                </Container>
            </section>
        </BrowserRouter>
    );
}

export default Banner;