import { Col, Container, Row } from "react-bootstrap";

const Footer = () => {
    return (
        <footer className="footer">
            <Container>
                <Row className="align-items-center">
                    <Col size={12} sm={6}>
                        <img src='./images/logos/zm.png' alt='LOGO' />
                    </Col>
                    <Col size={12} sm={6} className="text-center text-sm-end">
                        <div className="social-icon">
                            <a href='https://www.linkedin.com/in/zdravko-milinkovic-a07004225/' target="_blank" rel="noreferrer">
                                <img src='./images/icons/linkedin.svg' alt='LI' />
                            </a>
                            <a href='https://github.com/MilinkovicZ' target="_blank" rel="noreferrer">
                                <img src='./images/icons/github.svg' alt='GH' />
                            </a>
                            <a href='https://www.instagram.com/kozdrav_270/' target="_blank" rel="noreferrer">
                                <img src='./images/icons/instagram.svg' alt='IG' />
                            </a>
                        </div>
                    </Col>
                </Row>
            </Container>
        </footer>
    );
}

export default Footer;