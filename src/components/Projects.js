import { Col, Container, Row } from "react-bootstrap";
import Project from "./Project";

const Projects = () => {
    const projects = [
        {
            title: "Webshop",
            description: "Full-stack application for online shopping",
            imgUrl: "./images/projectIcons/webshop.jpg",
            GITURL: "https://github.com/MilinkovicZ/web-2-project"
        },
        {
            title: "Webshop v2.0",
            description: "Extended Webshop project including Paypal payment & map",
            imgUrl: "./images/projectIcons/webshopextended.jpg",
            GITURL: "https://github.com/MilinkovicZ/projekat-predmet"
        },
        {
            title: "Fitness Center",
            description: "Full-stack application for creating and administrating fitness centers",
            imgUrl: "./images/projectIcons/fitness.jpg",
            GITURL: "https://github.com/MilinkovicZ/Web-project"
        },
        {
            title: "Bank",
            description: "Full-stack application representing simple bank system",
            imgUrl: "./images/projectIcons/bank.jpg",
            GITURL: "https://github.com/MilinkovicZ/BankProject"
        },
        {
            title: "Converter",
            description: "Application for converting data between JSON, XML and SQL",
            imgUrl: "./images/projectIcons/converter.jpg",
            GITURL: "https://github.com/MilinkovicZ/res-tim20"
        },
        {
            title: "Load Balancer",
            description: "Console application for distributing traffic using Round robin algorithm",
            imgUrl: "./images/projectIcons/queue.jpg",
            GITURL: "https://github.com/MilinkovicZ/ikp_project"
        },
        {
            title: "Cryptography",
            description: "Application for data security including hashing, cryptography and certificates",
            imgUrl: "./images/projectIcons/crypto.jpg",
            GITURL: "https://github.com/MilinkovicZ/sbes-projekat"
        },
        {
            title: "Restaurant",
            description: "Frontend application of online restaurant",
            imgUrl: "./images/projectIcons/restaurant.jpg",
            GITURL: "https://github.com/MilinkovicZ/restaurant"
        }
    ];

    return (
        <section className="project" id="projects">
            <Container>
                <Row>
                    <Col>
                        <h2>Projects</h2>
                        <p>A few of my favorite projects:</p>
                        <Row>
                            {
                                projects.map((project, index) => {
                                    return (
                                        <Project
                                            key={index}
                                            {...project}
                                        />
                                    )   
                                })
                            }
                        </Row>
                    </Col>
                </Row>
            </Container>
            <img className="background-image-right" src="./images/bgs/grad2.png" alt="Right BG" />
        </section>
    );
}

export default Projects;