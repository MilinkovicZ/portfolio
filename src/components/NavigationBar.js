import { useEffect, useState } from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { BrowserRouter } from "react-router-dom";
import { HashLink } from "react-router-hash-link";

const NavigationBar = () => {
    const [activeLink, setActiveLink] = useState('home');
    const [scrolled, setScrolled] = useState(false);

    useEffect(() => {
        const onScroll = () => {
            if (window.scrollY > 50)
                setScrolled(true)
            else
                setScrolled(false)
        }

        window.addEventListener("scroll", onScroll);

        return () => window.removeEventListener("scroll", onScroll);
    }, [])

    const updateAciveLink = (linkValue) => {
        setActiveLink(linkValue);
    }

    return (
        <BrowserRouter>
            <Navbar expand="lg" className={scrolled ? "scrolled" : ""}>
                <Container>
                    <Navbar.Brand href="#home">
                        <img src='./images/logos/zm.png' alt='LOGO' />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav">
                        <span className='navbar-toggler-icon' />
                    </Navbar.Toggle>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="#home" onClick={() => updateAciveLink("home")} className={activeLink === "home" ? "active navbar-link" : "navbar-link"}>Home</Nav.Link>
                            <Nav.Link href="#skills" onClick={() => updateAciveLink("skills")} className={activeLink === "skills" ? "active navbar-link" : "navbar-link"}>Skills</Nav.Link>
                            <Nav.Link href="#projects" onClick={() => updateAciveLink("projects")} className={activeLink === "projects" ? "active navbar-link" : "navbar-link"}>Projects</Nav.Link>
                        </Nav>
                        <span className='navbar-text'>
                            <div className='social-icon'>
                                <a href='https://www.linkedin.com/in/zdravko-milinkovic-a07004225/' target="_blank" rel="noreferrer">
                                    <img src='./images/icons/linkedin.svg' alt='LI' />
                                </a>
                                <a href='https://github.com/MilinkovicZ' target="_blank" rel="noreferrer">
                                    <img src='./images/icons/github.svg' alt='GH' />
                                </a>
                                <a href='https://www.instagram.com/kozdrav_270/' target="_blank" rel="noreferrer">
                                    <img src='./images/icons/instagram.svg' alt='IG' />
                                </a>
                            </div>
                            <HashLink to='#connect'>
                                <button className="vvd"><span>Contact me</span></button>
                            </HashLink>
                        </span>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </BrowserRouter>
    );
}

export default NavigationBar;