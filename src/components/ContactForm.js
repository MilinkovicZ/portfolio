import { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";

const ContactForm = () => {
    const [formDetails, setFormDetails] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        message: ''
    });

    const [isFormValid, setIsFormValid] = useState(false);

    const [buttonText, setButtonText] = useState('Send');
    const [status, setStatus] = useState({});

    const onFormUpdate = (category, value) => {
        setFormDetails({
            ...formDetails,
            [category]: value
        })

        if (formDetails.email.trim().length === 0
            || formDetails.firstName.trim().length === 0
            || formDetails.lastName.trim().length === 0
            || formDetails.phone.trim().length === 0
            || formDetails.message.trim().length === 0)
            setIsFormValid(false);
        else
            setIsFormValid(true);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setButtonText("Sending...");
        let response = await fetch("https://milinkovicz.onrender.com/api/contact", {
            method: "POST",
            headers: {
                "Content-Type": "application/json;charset=utf-8",
            },
            body: JSON.stringify(formDetails),
        });
        setButtonText("Send");
        let result = await response.json();
        setFormDetails({
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            message: ''
        });
        setIsFormValid(false)
        if (result.code === 200) {
            setStatus({ succes: true, message: 'Message sent successfully' });
        }
    };

    return (
        <section className="contact" id="connect">
            <Container>
                <Row className="align-items-center">
                    <Col md={6}>
                        <img src="./images/bgs/contact.png" alt="Contact me" />
                    </Col>
                    <Col md={6}>
                        <h2>Get in touch</h2>
                        <form onSubmit={handleSubmit}>
                            <Row>
                                <Col size={12} sm={6} className="px-1">
                                    <input type="text" value={formDetails.firstName} placeholder="First Name" onChange={(e) => onFormUpdate('firstName', e.target.value)} />
                                </Col>
                                <Col size={12} sm={6} className="px-1">
                                    <input type="text" value={formDetails.lastName} placeholder="Last Name" onChange={(e) => onFormUpdate('lastName', e.target.value)} />
                                </Col>
                                <Col size={12} sm={6} className="px-1">
                                    <input type="email" value={formDetails.email} placeholder="Email Address" onChange={(e) => onFormUpdate('email', e.target.value)} />
                                </Col>
                                <Col size={12} sm={6} className="px-1">
                                    <input type="tel" value={formDetails.phone} placeholder="Phone No." onChange={(e) => onFormUpdate('phone', e.target.value)} />
                                </Col>
                                <Col size={12} className="px-1">
                                    <textarea rows="6" value={formDetails.message} placeholder="Message" onChange={(e) => onFormUpdate('message', e.target.value)}></textarea>
                                    <button disabled={!isFormValid} type="submit"><span>{buttonText}</span></button>
                                </Col>
                                {
                                    status.message &&
                                    <p className={status.success === false ? "danger" : "success"}>{status.message}</p>
                                }
                            </Row>
                        </form>
                    </Col>
                </Row>
            </Container>
        </section>
    );
}

export default ContactForm;